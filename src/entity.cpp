#include "entity.hpp"

#include "map.hpp"
#include "dropped_items.hpp"
#include <algorithm>
#include <vector>

Entity::Entity(std::pair<int, int> position_init, Map* map, char style,
               int hp_init, double interaction_range, std::vector<std::shared_ptr<Item>> inventory, int attack)
    : hp_(hp_init), max_hp_(hp_init), map_(map), pos_(position_init), style_(style), interaction_range_(interaction_range),
      inventory_(inventory), base_attack_(attack), weapon_(std::shared_ptr<Weapon>(new Weapon())),
      armor_(std::shared_ptr<Armor>(new Armor())) {}

Entity::~Entity() {}

void Entity::set_pos(std::pair<int, int> new_position) {
    map_->set_position(pos_, new_position);
    pos_ = new_position;
}

// Gets an integer indicating direction: 1 = North, 2 = East...
bool Entity::move(int direction) { 
    auto target = dir_to_pos(direction);

    if (this->map_->is_accessible(target)) {
        set_pos(target);
        return true;
    }
    return false;
}
bool Entity::player_in_range() {
    std::pair<int, int> playerPos = map_->get_player()->get_pos();
    return distance(playerPos, pos_) <= interaction_range_;
}

void Entity::get_hit(int damage) {
    hp_ -= std::max(damage / armor_->get_defence_boost(), 1);
    if (hp_ <= 0){
        die();
    }
}

void Entity::heal(int hp) {
    hp_ = std::min(hp_ + hp, max_hp_);
}

void Entity::die() {
    map_->delete_entity(pos_);
    if (inventory_.size() > 0)
        map_->add_entity(std::shared_ptr<Entity>(new DroppedItems(pos_, map_, inventory_)));
}

std::pair<int, int> Entity::get_pos() const {
    return pos_;
}

char Entity::get_style() const {
    return style_;
}

void Entity::act() {
    // default entity does nothing
}

Position Entity::dir_to_pos(int direction) const {
    int x, y;
    switch(direction) {
        case 1:
        x = pos_.first;
        y = pos_.second - 1;
        break;

        case 2:
        x = pos_.first + 1;
        y = pos_.second;
        break;

        case 3:
        x = pos_.first;
        y = pos_.second + 1;
        break;

        case 4:
        x = pos_.first - 1;
        y = pos_.second;
        break;
    }
    return Position(x, y);
}

void Entity::interact() {

}

int Entity::get_hp() const {
    return hp_;
}

int Entity::get_max_hp() const {
    return max_hp_;
}

double Entity::get_range() const {
    return interaction_range_;
}

int Entity::get_attack() const {
    return base_attack_ + weapon_->get_attack_boost();
}

void Entity::take(std::shared_ptr<Item> item) {
    take(std::vector<std::shared_ptr<Item>>{item});
}

void Entity::take(std::vector<std::shared_ptr<Item>> items) {
    for (auto i : items) {
        inventory_.push_back(i);
    }
}

void Entity::remove_item(Item* item) {
    for (auto i = inventory_.begin(); i != inventory_.end(); ++i) {
        if (i->get() == item) {
            inventory_.erase(i);
            break;
        }
    }
}

void Entity::remove_item(std::shared_ptr<Item> item) {
    auto i = std::find(inventory_.begin(), inventory_.end(), item);
    if (i != inventory_.end()) {
        if (weapon_ == item) {
            weapon_ = std::shared_ptr<Weapon>(new Weapon());
        }
        else if (armor_ == item) {
            armor_ = std::shared_ptr<Armor>(new Armor());
        }
        inventory_.erase(i);
    }
}

std::shared_ptr<Armor> Entity::get_armor() {
    return armor_;
}

std::shared_ptr<Weapon> Entity::get_weapon() {
    return weapon_;
}

void Entity::equip_armor(Armor* armor) {
    for (auto i : inventory_) {
        if (i.get() == armor) {
            armor_ = std::static_pointer_cast<Armor>(i);
            break;
        }
    }
}

void Entity::equip_weapon(Weapon* weapon) {
    for (auto i : inventory_) {
        if (i.get() == weapon) {
            weapon_ = std::static_pointer_cast<Weapon>(i);
            break;
        }
    }
}

const std::vector<std::shared_ptr<Item>>& Entity::get_inventory() const {
    return inventory_;
}