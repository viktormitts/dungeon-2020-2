#ifndef WITCH_HPP
#define WITCH_HPP

#include "enemy.hpp"
class Witch : public Enemy {
public:
    Witch(std::pair<int, int> position_init, Map* map, std::vector<std::shared_ptr<Item>> inventory);
    virtual ~Witch();
        
};
#endif