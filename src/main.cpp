#include "game.hpp"
#include "map.hpp"
#include <iostream>
#include <algorithm>
#include <sstream>
#include <fstream>
#include <SFML/Graphics.hpp>

int main() {
    Map map;
    std::fstream file("../map.txt", std::fstream::in);
    map.read_from_stream(file);
    file.flush();
    file.close();
    Game game(map);

    std::string itemUseBuffer;

    sf::RenderWindow window(sf::VideoMode(800, 600), "dungeon");

    // how to move texture creation elsewhere
    sf::Texture texturePlayer;

    sf::Texture textureWall;
    sf::Texture textureEmpty;

    sf::Texture textureSoldier;
    sf::Texture textureWitch;
    
    sf::Texture textureLoot;
    sf::Texture textureDoor;

    sf::Texture textureSword;
    sf::Texture textureCoolSword;

    sf::Texture textureSoup;
    sf::Texture textureMeat;

    texturePlayer.loadFromFile("../textures/player.png");

    textureWall.loadFromFile("../textures/wall.png");
    textureEmpty.loadFromFile("../textures/empty.png");

    textureSoldier.loadFromFile("../textures/soldier.png");
    textureWitch.loadFromFile("../textures/witch.png");

    textureLoot.loadFromFile("../textures/loot.png");
    textureDoor.loadFromFile("../textures/door.png");

    textureSword.loadFromFile("../textures/sword.png");
    textureCoolSword.loadFromFile("../textures/cool_sword.png");

    textureSoup.loadFromFile("../textures/soup.png");
    textureMeat.loadFromFile("../textures/meat.png");

    sf::Font font;
    font.loadFromFile("../Castoro-Regular.ttf");

    while (window.isOpen()) {
        std::string cmd;

        sf::Event event;
        while (window.pollEvent(event)) {
            switch (event.type) {
                case sf::Event::Closed:
                    window.close();
                    break;
                case sf::Event::KeyPressed:
                    switch (event.key.code) {
                        case sf::Keyboard::A:
                            cmd = "a";
                            break;
                        case sf::Keyboard::S:
                            cmd = "s";
                            break;
                        case sf::Keyboard::D:
                            cmd = "d";
                            break;
                        case sf::Keyboard::W:
                            cmd = "w";
                            break;
                        case sf::Keyboard::Num0:
                            itemUseBuffer.push_back('0');
                            break;
                        case sf::Keyboard::Num1:
                            itemUseBuffer.push_back('1');
                            break;
                        case sf::Keyboard::Num2:
                            itemUseBuffer.push_back('2');
                            break;
                        case sf::Keyboard::Num3:
                            itemUseBuffer.push_back('3');
                            break;
                        case sf::Keyboard::Num4:
                            itemUseBuffer.push_back('4');
                            break;
                        case sf::Keyboard::Num5:
                            itemUseBuffer.push_back('5');
                            break;
                        case sf::Keyboard::Num6:
                            itemUseBuffer.push_back('6');
                            break;
                        case sf::Keyboard::Num7:
                            itemUseBuffer.push_back('7');
                            break;
                        case sf::Keyboard::Num8:
                            itemUseBuffer.push_back('8');
                            break;
                        case sf::Keyboard::Num9:
                            itemUseBuffer.push_back('9');
                            break;
                        case sf::Keyboard::E:
                            cmd = "u" + itemUseBuffer;
                            itemUseBuffer.clear();
                            break;
                        case sf::Keyboard::Q:
                            itemUseBuffer.clear();
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

        if (cmd.length() > 0)
            game.update(cmd);

        window.clear(sf::Color::Black);

        auto playerPos = map.get_player()->get_pos();

        // the leftmost tile to draw
        auto leftBound = std::max(playerPos.first - 14, 0);
        // the rightmost tile to draw
        auto rightBound = std::min(size_t(playerPos.first + 15), map.get_grid()[0].size());
        auto upBound = std::max(playerPos.second - 9, 0);
        auto downBound = std::min(size_t(playerPos.second + 10), map.get_grid().size());

        sf::Sprite sprite;
        // draw tiles
        for (unsigned i = upBound; i < downBound; ++i) {
            for (unsigned j = leftBound; j < rightBound; ++j) {
                switch (map.get_grid()[i][j]->get_style()) {
                    case '#':
                        sprite.setTexture(textureWall);
                        break;
                    case '.':
                        sprite.setTexture(textureEmpty);
                        break;
                    default:
                        break;
                }
                sprite.setPosition((j - leftBound) * 32, (i - upBound) * 32);
                window.draw(sprite);
            }
        }

        // Draw entities
        for (auto i : map.get_entities()) {
            switch (i.second->get_style()) {
                case 'P':
                    sprite.setTexture(texturePlayer);
                    break;
                case 'S':
                    sprite.setTexture(textureSoldier);
                    break;
                case 'W':
                    sprite.setTexture(textureWitch);
                    break;
                case 'I':
                    sprite.setTexture(textureLoot);
                    break;
                case 'O':
                    sprite.setTexture(textureDoor);
                    break;
                default:
                    break;
            }
            auto x = (i.first.first - leftBound) * 32;
            auto y = (i.first.second - upBound) * 32;
            sprite.setPosition(x, y);
            window.draw(sprite);

            // health bar
            sf::RectangleShape bg(sf::Vector2f(32.f, 5.f));
            bg.setFillColor(sf::Color::Black);
            sf::RectangleShape bar(sf::Vector2f(((double)i.second->get_hp() / (double)i.second->get_max_hp()) * 32.f, 5.f));
            bar.setFillColor(sf::Color::Green);
            bg.setPosition(x, y - 5);
            bar.setPosition(x, y - 5);
            window.draw(bg);
            window.draw(bar);
        }

        // Draw inventory
        unsigned spriteX = 0;
        unsigned index = 0;
        sf::Text text;
        std::stringstream indexString;
        text.setFont(font);
        text.setCharacterSize(14);
        text.setFillColor(sf::Color::White);
        for (auto i : map.get_player()->get_inventory()) {
            auto name = i->get_name();
            if (name == "Sword") {
                sprite.setTexture(textureSword);
            }
            else if (name == "Cool Sword") {
                sprite.setTexture(textureCoolSword);
            }
            else if (name == "Soup") {
                sprite.setTexture(textureSoup);
            }
            else if (name == "Meat") {
                sprite.setTexture(textureMeat);
            }
            sprite.setPosition(spriteX, window.getSize().y - 32);
            window.draw(sprite);

            // key
            indexString << index;
            text.setString(indexString.str());
            indexString.str("");
            text.setPosition(spriteX, window.getSize().y - 32);
            window.draw(text);
            
            ++index;
            spriteX += 32;
        }

        text.setCharacterSize(100);
        text.setPosition(0, 0);

        // Draw win or lose text
        if (map.get_player()->get_won()) {
            text.setString("You Win");
            window.draw(text);
        }
        else if (map.get_player()->get_hp() <= 0) {
            text.setString("You Suck");
            text.setFillColor(sf::Color::Red);
            window.draw(text);
        }

        window.display();
    }
    return 0;
}