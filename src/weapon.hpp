#ifndef WEAPON_HPP
#define WEAPON_HPP

#include "equipment.hpp"

class Weapon : public Equipment {
public:
    Weapon();
    Weapon(std::string name, int attackBoost, int healthBoost);
    virtual ~Weapon();

    virtual void use(Entity& user);

    // i'm just gonna add different weapons like this
    static Weapon sword();
    static Weapon cool_sword();
};

#endif