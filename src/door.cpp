#include "door.hpp"

#include "map.hpp"

Door::Door(std::pair<int, int> position_init, Map* map) : Entity(position_init, map, 'O', -1) {}

Door::~Door() {}

void Door::interact() {
    map_->get_player()->win();
}