#ifndef EQUIPMENT_HPP
#define EQUIPMENT_HPP
#include "item.hpp"

class Equipment : public Item {

public:
    Equipment(std::string name, int attackBoost, int defenceBoost);
    virtual ~Equipment();  
    int get_attack_boost() const;
    int get_defence_boost() const;

private:
    int attackBoost_;
    int defenceBoost_;
};


#endif