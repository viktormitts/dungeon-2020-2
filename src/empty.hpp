#ifndef EMPTY_HPP
#define EMPTY_HPP

#include "tile.hpp"

class Empty : public Tile {
public:
    Empty(std::pair<int, int> position_init);
    ~Empty();
};

#endif