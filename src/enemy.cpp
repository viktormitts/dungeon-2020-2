#include "enemy.hpp"

#include "entity.hpp"
#include "item.hpp"
#include <vector>
#include <memory>

Enemy::Enemy(std::pair<int, int> position_init, Map* map, char style, int hp_init, double interaction_range, std::vector<std::shared_ptr<Item>> inventory, int attack)
    : Entity(position_init, map, style, hp_init, interaction_range, inventory, attack) {}

Enemy::~Enemy() {}

//Enemy chooses random direction by default
void Enemy::act() {
    if (!player_in_range()) {
        srand((unsigned) time(0));
        int ranDir = (rand() % 4) + 1;
        move(ranDir);
    } else {
        attack();
    }

}

void Enemy::attack() {
    map_->get_player()->get_hit(get_attack());
}

void Enemy::interact() {
    get_hit(map_->get_player()->get_attack());
}
