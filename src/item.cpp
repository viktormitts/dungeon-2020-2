#include "item.hpp"

#include "entity.hpp"

Item::Item(std::string name) : name_(name) {}

Item::~Item() {}

std::string Item::get_name() const {
    return name_;
}

void Item::use(Entity& user) {

}

bool operator==(Item a, Item b) {
    return a.get_name() == b.get_name();
}