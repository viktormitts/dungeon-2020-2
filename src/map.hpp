#ifndef MAP_HPP
#define MAP_HPP

#include <vector>
#include <string>
#include <memory>
#include <cmath>
#include "tile.hpp"
#include "player.hpp"
#include <map>

class Entity;

class Map {
public:
    Map();
    ~Map();
    void init_map(std::vector<std::vector<std::string>> grid_init);
    void read_from_stream(std::istream& stream);
    void print_map();

    /**
     * @brief Sets the position of an entity.
     * 
     * @param start the current position of the entity
     * @param destination the position the entity will be moved to
     * 
     * @returns @c true if there is an entity at @c start, @c false otherwise.
     * 
     * @remarks very stupid
    */
    bool set_position(Position start, Position destination);

    const std::vector<std::vector<Tile*>>& get_grid() const;
    const std::map<std::pair<int, int>, std::shared_ptr<Entity>>& get_entities() const;
    void set_grid(std::vector<std::vector<Tile*>> new_grid);
    bool move(std::pair<int, int> position1, std::pair<int, int> position2);
    bool is_accessible(Position pos) const;
    bool is_accessible(int x, int y) const;
    std::shared_ptr<Player> get_player();

    /**
     * @brief Add an entity to the map.
     * 
     * @param entity The entity to add
     * 
     * @returns true if no entity was already there
     */
    bool add_entity(std::shared_ptr<Entity> entity);

    /**
     * @brief Delete an entity from the map.
     * 
     * @param position The position of the entity to delete (sorry)
     * 
     * @returns true if there was an entity at the position, false otherwise.
     */
    bool delete_entity(Position position);
private:
    std::vector<std::vector<Tile*>> grid;
    std::map<std::pair<int, int>, std::shared_ptr<Entity>> entities_;
    std::shared_ptr<Player> player_;
    /*std::vector<Npc*> npcs;
    std::vector<Enemy*> enemies;*/
};

double distance(Position a, Position b);

#endif