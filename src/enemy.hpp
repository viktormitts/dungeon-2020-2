#ifndef ENEMY_HPP
#define ENEMY_HPP

#include "entity.hpp"
#include "map.hpp"

class Enemy : public Entity {
public:
    Enemy(std::pair<int, int> position_init, Map* map, char style, int hp_init,
        double interaction_range = 1.0, std::vector<std::shared_ptr<Item>> inventory = std::vector<std::shared_ptr<Item>>(), int attack = 1);
    virtual ~Enemy();

    /**
     * @brief Make this enemy perform an action. The default implementation moves in a random direction.
     * 
     * maybe change the default behavior idk i'm just seeing if it works at all
    */
    virtual void act();

    /**
     * @brief The player attacks the enemy.
     */
    virtual void interact();

protected:
    virtual void attack();

};

#endif