#ifndef GAME_HPP
#define GAME_HPP

#include "map.hpp"

class Game {
public:
    Game(Map& map);
    ~Game();
    void update(std::string cmd);
private:
    Map& map_;
    void render();
};

#endif