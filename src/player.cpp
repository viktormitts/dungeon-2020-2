#include "player.hpp"

#include "map.hpp"
#include "entity.hpp"

Player::Player(std::pair<int, int> position_init, Map* map)
    : Entity(position_init, map, 'P', 10, 1.0), won_(false) {}

Player::~Player() {}

void Player::win() {
    won_ = true;
}

bool Player::get_won() {
    return won_;
}