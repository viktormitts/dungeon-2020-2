#include "food.hpp"

Food::Food(std::string name, int healAmount) : Item(name), healAmount_(healAmount) {}

Food::~Food() {}

int Food::getHealAmount() {
    return healAmount_;
}

void Food::use(Entity& user) {
    user.heal(healAmount_);
    user.remove_item(this);
}

Food Food::soup() {
    return Food("Soup", 3);
}

Food Food::meat() {
    return Food("Meat", 5);
}