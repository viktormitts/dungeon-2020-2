#ifndef TILE_HPP
#define TILE_HPP

#include <utility>
#include <string>

typedef std::pair<int, int> Position;

class Tile {
public:
    Tile(std::pair<int, int> position_init, bool accessible_init, char style_init);
    ~Tile();
    std::pair<int, int> get_position();
    bool is_accessible();
    char get_style();
protected:
    bool accessible;
    char style;
    std::pair<int, int> position;
};

#endif