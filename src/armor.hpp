#ifndef ARMOR_HPP
#define ARMOR_HPP

#include "equipment.hpp"

class Armor : public Equipment {
public:
    Armor();
    Armor(std::string name, int attackBoost, int healthBoost);
    virtual ~Armor();

    virtual void use(Entity& user);

    // i'm just gonna add armors like this
    static Armor knight_armor();
    static Armor indestructo_suit();
};

#endif