#ifndef SOLDIER_HPP
#define SOLDIER_HPP


#include "enemy.hpp"
class Soldier : public Enemy {
public:
    Soldier(std::pair<int, int> position_init, Map* map, std::vector<std::shared_ptr<Item>> inventory);
    virtual ~Soldier();
    /**
     * A soldier moves only vertically.
     * If collides, changes direction.
     * 
     **/
    void act();
private:
    bool dir_right_not_left_ = true;
    int direction_ = 2;
};

#endif