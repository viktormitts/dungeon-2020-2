#include "tile.hpp"

Tile::Tile(std::pair<int, int> position_init, bool accessible_init, char style_init) : position(position_init), accessible(accessible_init), style(style_init) {}

Tile::~Tile() {}

std::pair<int, int> Tile::get_position() {
    return position;
}

bool Tile::is_accessible() {
    return accessible;
}

char Tile::get_style() {
    return style;
}

