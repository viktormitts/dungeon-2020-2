#ifndef GOLD_HPP
#define GOLD_HPP
#include "item.hpp"

class Gold : public Item {
public:
    Gold(std::string name, int amount);
    virtual ~Gold();
    int getAmount();
private:
    int amount_;
};


#endif