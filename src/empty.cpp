#include "empty.hpp"

#include "tile.hpp"

Empty::Empty(std::pair<int, int> position_init) : Tile(position_init, true, '.') {}

Empty::~Empty() {}