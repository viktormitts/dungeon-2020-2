#include "armor.hpp"

#include "entity.hpp"

Armor::Armor() : Armor("None", 0, 1) {}

Armor::Armor(std::string name, int attackBoost, int healthBoost)
    : Equipment(name, attackBoost, healthBoost) {}

Armor::~Armor() {}

Armor Armor::knight_armor() {
    return Armor("Knight Armor", 0, 2);
}

Armor Armor::indestructo_suit() {
    return Armor("Indestructo-Suit", 0, 10);
}

void Armor::use(Entity& user) {
    user.equip_armor(this);
}