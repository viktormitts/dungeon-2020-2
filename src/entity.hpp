#ifndef ENTITY_HPP
#define ENTITY_HPP

#include "tile.hpp"
#include "weapon.hpp"
#include "armor.hpp"
#include "item.hpp"
#include <utility>
#include <vector>
#include <memory>

class Map;

class Entity {
public:
    Entity(std::pair<int, int> position_init, Map* map, char style, int hp_init, double interaction_range = 1.0,
           std::vector<std::shared_ptr<Item>> inventory = std::vector<std::shared_ptr<Item>>(), int attack = 1);
    virtual ~Entity();

    /**
     * @brief Sets the position of the entity.
     * 
     * @param new_position The new position it will move to.
    */
    void set_pos(std::pair<int, int> new_position);

    /**
     * @brief Returns true if player is in afjacent tile vertically horizontally or diagonally.
     * 
     *
    **/

    virtual void get_hit(int damage);
    virtual void heal(int hp);

    virtual void die();

    bool player_in_range();
    
    std::pair<int, int> get_pos() const;
    virtual bool move(int direction);
    virtual void act();
    char get_style() const;
    Position dir_to_pos(int dir) const;

    /**
     * @brief The player interacts with this entity. The result depends on the entity type.
     */
    virtual void interact();

    int get_hp() const;
    int get_max_hp() const;
    int get_attack() const;
    double get_range() const;
    void take(std::shared_ptr<Item> item);
    void take(std::vector<std::shared_ptr<Item>> items);
    void remove_item(std::shared_ptr<Item> item);
    void remove_item(Item* item);
    void equip_weapon(Weapon* weapon);
    void equip_armor(Armor* armor);
    std::shared_ptr<Weapon> get_weapon();
    std::shared_ptr<Armor> get_armor();
    const std::vector<std::shared_ptr<Item>>& get_inventory() const;
protected:
    char style_;
    Map* map_;
    std::vector<std::shared_ptr<Item>> inventory_;
    int base_attack_;
    int max_hp_;
private:
    int hp_;
    std::pair<int, int> pos_;
    double interaction_range_;
    std::shared_ptr<Weapon> weapon_;
    std::shared_ptr<Armor> armor_;
};

#endif