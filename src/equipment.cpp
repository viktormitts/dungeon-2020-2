#include "equipment.hpp"

Equipment::Equipment(std::string name, int attackBoost, int defenceBoost)
    : Item(name), attackBoost_(attackBoost), defenceBoost_(defenceBoost) {}

Equipment::~Equipment() {}

int Equipment::get_attack_boost() const {
    return attackBoost_;
}

int Equipment::get_defence_boost() const {
    return defenceBoost_;
}

