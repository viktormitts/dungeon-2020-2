#include "dropped_items.hpp"

#include "map.hpp"

DroppedItems::DroppedItems(Position position, Map* map, std::vector<std::shared_ptr<Item>> items)
    : Entity(position, map, 'I', -1, 1, items) {}

DroppedItems::~DroppedItems() {}

void DroppedItems::interact() {
    map_->get_player()->take(inventory_);
    die();
    map_->get_player()->set_pos(get_pos());
}

void DroppedItems::get_hit() {
    // items do not get hit
}