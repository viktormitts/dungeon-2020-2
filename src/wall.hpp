#ifndef WALL_HPP
#define WALL_HPP

#include "tile.hpp"

class Wall : public Tile {
public:
    Wall(std::pair<int, int> position_init);
    ~Wall();
};

#endif