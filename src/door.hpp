#ifndef DOOR_HPP
#define DOOR_HPP

#include "entity.hpp"

class Door : public Entity {
public:
    Door(std::pair<int, int> position_init, Map* map);
    virtual ~Door();

    virtual void interact();
};

#endif