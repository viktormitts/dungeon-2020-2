#include "gold.hpp"

Gold::Gold(std::string name, int amount) : Item(name), amount_(amount) {}

Gold::~Gold() {}

int Gold::getAmount() {
    return amount_;
}
