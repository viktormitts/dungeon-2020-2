#ifndef ITEM_HPP
#define ITEM_HPP

#include <string>

class Entity;

class Item {
public:
    Item(std::string name);
    virtual ~Item();
    std::string get_name() const;
    virtual void use(Entity& user);
private:
    std::string name_;
    
};

bool operator==(Item a, Item b);

#endif