#include "witch.hpp"
#include "enemy.hpp"


Witch::Witch(std::pair<int, int> position_init, Map* map, std::vector<std::shared_ptr<Item>> inventory)
    : Enemy(position_init, map, 'W', 3, 2.0, inventory) {}

Witch::~Witch(){}