#include "soldier.hpp"

#include "enemy.hpp"


Soldier::Soldier(std::pair<int, int> position_init, Map* map, std::vector<std::shared_ptr<Item>> inventory)
    : Enemy(position_init, map, 'S', 3, 1.0, inventory) {}

void Soldier::act() {
    if (!player_in_range()) {
        if (!move(direction_)) {
            if (dir_right_not_left_){
                direction_ = 4;
            } else {
                direction_ = 2;
            }
            dir_right_not_left_ = !dir_right_not_left_;
        }
    } else {
        attack();
    }
}

Soldier::~Soldier(){}