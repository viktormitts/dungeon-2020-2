#include "map.hpp"

#include <vector>
#include <iostream>
#include <memory>
#include <sstream>
#include "tile.hpp"
#include "wall.hpp"
#include "empty.hpp"
#include "door.hpp"
#include "enemy.hpp"
#include "dropped_items.hpp"
#include "weapon.hpp"
#include "food.hpp"
#include "soldier.hpp"
#include "witch.hpp"

Map::Map() {}

Map::~Map() {
    for (auto i : grid) {
        for (auto j : i) {
            delete j;
        }
    }
}

void Map::init_map(std::vector<std::vector<std::string>> grid_init) {
    //initialize map based on string input
    for(int i = 0; i < grid_init.size(); i++) {
        std::vector<Tile*> temp;
        for(int j = 0; j < grid_init[0].size(); j++) {
            // why not put actual tiles in this parameter instead of strings?
            if(grid_init[i][j] == "wall")
                temp.push_back(new Wall(std::make_pair(j, i)));
            else if(grid_init[i][j] == "empty")
                temp.push_back(new Empty(std::make_pair(j, i)));
            else if(grid_init[i][j] == "door") {
                temp.push_back(new Empty(std::make_pair(j, i)));
                add_entity(std::shared_ptr<Door>(new Door(Position(j, i), this)));
            }
            else if(grid_init[i][j] == "player") {
                temp.push_back(new Empty(Position(j, i)));
                entities_[Position(j, i)] = player_ = std::shared_ptr<Player>(new Player(Position(j, i), this));
            }
            else if(grid_init[i][j] == "enemy") {
                temp.push_back(new Empty(Position(j, i)));
                entities_[Position(j, i)] = std::shared_ptr<Entity>(new Soldier(Position(j, i), this, std::vector<std::shared_ptr<Item>>{std::make_shared<Food>(Food::soup())}));
            }
            else if(grid_init[i][j] == "item") {
                temp.push_back(new Empty(Position(j, i)));
                entities_[Position(j, i)] = std::shared_ptr<Entity>(new DroppedItems(Position(j, i), this, std::vector<std::shared_ptr<Item>>{std::shared_ptr<Weapon>(new Weapon(Weapon::sword()))}));//std::make_shared<Weapon>(Weapon::sword())}));
            }
        }
        grid.push_back(temp);
    }
}

void Map::read_from_stream(std::istream& stream) {
    for (unsigned i = 0; stream.good(); ++i) {
        std::vector<Tile*> row;
        std::string line;
        stream >> line;
        std::stringstream ss(line);
        for (unsigned j = 0; ss.good(); ++j) {
            char thing;
            ss >> thing;
            switch (thing) {
                case '#':
                    row.push_back(new Wall(Position(j, i)));
                    break;
                case '.':
                    row.push_back(new Empty(Position(j, i)));
                    break;
                case 'P':
                    row.push_back(new Empty(Position(j, i)));
                    entities_[Position(j, i)] = player_ = std::shared_ptr<Player>(new Player(Position(j, i), this));
                    break;
                case 'O':
                    row.push_back(new Empty(std::make_pair(j, i)));
                    add_entity(std::shared_ptr<Door>(new Door(Position(j, i), this)));
                    break;
                case 'S':
                    row.push_back(new Empty(Position(j, i)));
                    entities_[Position(j, i)] = std::shared_ptr<Entity>(new Soldier(
                        Position(j, i), this, std::vector<std::shared_ptr<Item>>{std::make_shared<Food>(Food::soup())}));
                    break;
                case 'W':
                    row.push_back(new Empty(Position(j, i)));
                    entities_[Position(j, i)] = std::shared_ptr<Entity>(new Witch(
                        Position(j, i), this, std::vector<std::shared_ptr<Item>>{std::make_shared<Food>(Food::meat())}));
                    break;
                case 'I':
                    row.push_back(new Empty(Position(j, i)));
                    entities_[Position(j, i)] = std::shared_ptr<Entity>(
                        new DroppedItems(Position(j, i), this, std::vector<std::shared_ptr<Item>>{std::shared_ptr<Weapon>(
                            new Weapon(Weapon::sword()))}));
                    break;
                default:
                    row.push_back(new Empty(Position(j, i)));
                    break;
            }
        }
        grid.push_back(row);
    }
}

void Map::print_map() {
    for(const auto& i : grid) {
        for(const auto& j: i) {
            auto entity = entities_.find(j->get_position());
            if (entity != entities_.end()) {
                std::cout << entity->second->get_style();
            }
            else {
                std::cout << j->get_style();
            }
        }
        std::cout << std::endl;
    }
}

const std::vector<std::vector<Tile*>>& Map::get_grid() const {
    return grid;
}

void Map::set_grid(std::vector<std::vector<Tile*>> new_grid) {
    grid = new_grid;
}

bool Map::set_position(Position start, Position destination) {
    auto it = entities_.find(start);
    if (it != entities_.end()) {
        entities_[destination] = it->second;
        entities_.erase(it);
        return true;
    }
    return false;
}

bool Map::is_accessible(Position pos) const {
    return is_accessible(pos.first, pos.second);
}

bool Map::is_accessible(int x, int y) const {
    if (grid[y][x]->is_accessible() && entities_.find(std::make_pair(x,y)) == entities_.end()) {
        return true;
    }
    return false;
}

std::shared_ptr<Player> Map::get_player() {
    return player_;
}

bool Map::move(std::pair<int, int> from, std::pair<int, int> to) {
    int x1 = from.first;
    int y1 = from.second;
    int x2 = to.first;
    int y2 = to.second;
    //move by changing two tiles
    if(grid[x2][y2]->is_accessible()) {
        //change tiles by using temp
        Tile* temp = grid[x2][y2];
        grid[x2][y2] = grid[x1][y1];
        grid[x1][y1] = temp;
        return true;
    }
    return false;
}

const std::map<std::pair<int, int>, std::shared_ptr<Entity>>& Map::get_entities() const {
    return entities_;
}

bool Map::add_entity(std::shared_ptr<Entity> entity) {
    auto e = entities_.find(entity->get_pos());
    if (e == entities_.end()) {
        entities_[entity->get_pos()] = entity;
        return true;
    }
    return false;
}

bool Map::delete_entity(Position position) {
    auto e = entities_.find(position);
    if (e != entities_.end()) {
        entities_.erase(e);
        return true;
    }
    return false;
}

double distance(Position a, Position b) {
    auto x = a.first - b.first;
    auto y = a.second - b.second;
    x *= x; y *= y;
    return sqrt((double)x + (double)y);
}