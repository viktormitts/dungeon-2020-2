#ifndef FOOD_HPP
#define FOOD_HPP

#include "item.hpp"
#include "entity.hpp"

class Food : public Item {

public:
    Food(std::string name, int healAmount);
    virtual ~Food();
    int getHealAmount();
    virtual void use(Entity& user);

    static Food soup();
    static Food meat();
    
private:
    int healAmount_;

};

#endif