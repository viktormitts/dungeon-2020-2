#ifndef DROPPED_ITEMS_HPP
#define DROPPED_ITEMS_HPP

#include "entity.hpp"
#include "item.hpp"
#include <vector>

class DroppedItems : public Entity {
public:
    DroppedItems(Position position, Map* map, std::vector<std::shared_ptr<Item>> items);
    virtual ~DroppedItems();

    virtual void interact();

    virtual void get_hit();
};

#endif