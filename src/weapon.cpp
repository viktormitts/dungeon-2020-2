#include "weapon.hpp"

#include "entity.hpp"

Weapon::Weapon() : Weapon("None", 0, 0) {}

Weapon::Weapon(std::string name, int attackBoost, int healthBoost)
    : Equipment(name, attackBoost, healthBoost) {}

Weapon::~Weapon() {}

void Weapon::use(Entity& user) {
    user.equip_weapon(this);
}

Weapon Weapon::sword() {
    return Weapon("Sword", 1, 0);
}

Weapon Weapon::cool_sword() {
    return Weapon("Cool Sword", 2, 0);
}