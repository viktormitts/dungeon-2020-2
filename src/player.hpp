#ifndef PLAYER_HPP
#define PLAYER_HPP

#include "entity.hpp"
#include <utility>

class Map;

class Player : public Entity {
public:
    Player(std::pair<int, int> position_init, Map* map);
    Player(int hp_init, std::pair<int, int> position_init, Map* map);
    virtual ~Player();

    void win();

    bool get_won();
private:
    bool won_;
};

#endif