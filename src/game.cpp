#include "game.hpp"

#include <iostream>
#include <sstream>
#include "map.hpp"

Game::Game(Map& map) : map_(map) {}

Game::~Game() {}

void Game::update(std::string cmd) {
    if (cmd.length() == 0) {
        return;
    }
    std::stringstream stream(cmd);
    auto entities = map_.get_entities();
    if (map_.get_player()->get_hp() > 0) {
        int direction = -1;
        char action;
        stream >> action;
        switch (action) {
            case 'w':
                direction = 1;
                break;
            case 'a':
                direction = 4;
                break;
            case 's':
                direction = 3;
                break;
            case 'd':
                direction = 2;
                break;
            case 'u':
                if (stream.peek() != EOF) {
                    int index;
                    stream >> index;
                    auto inventory = map_.get_player()->get_inventory();
                    if (index >= 0 && index < inventory.size()) {
                        inventory[index]->use(*map_.get_player());
                    }
                }
            default:
                break;
        }
        if (direction != -1) {
            auto target = map_.get_player()->dir_to_pos(direction);
            auto entity = entities.find(target);
            if (entity != entities.end()) {
                entity->second->interact();
            }
            else {
                map_.get_player()->move(direction);
            }
        }
    }

    // enemy action
    // we copy them all so that the iterator is not invalidated
    // while positions are updated in the dumbest way possible.
    // i hope it's not too inefficient
    for (auto i : entities) {
        i.second->act();
    }
}

void Game::render() {
    map_.print_map();
}