# Dungeon

## Building

### Requirements

  * CMake >= 3.1
  * SFML 2.5.1

### Instructions

```
mkdir build
cd build
cmake ..
make
```

## How to Play

Start the game by running `./dungeon` while in `build`. Use WASD to move. Attack enemies and pick up items by moving into them. Use an item by typing its index in the list (shown next to the item on the inventory bar) and pressing E. Clear the typed numbers by pressing Q. Win by getting to the door.
