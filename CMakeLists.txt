cmake_minimum_required(VERSION 3.1)
project(dungeon VERSION 0.1.0)

include(CTest)
enable_testing()

include_directories(${PROJECT_SOURCE_DIR})
file(GLOB SOURCES src/*.cpp)
find_package(SFML 2.5.1 COMPONENTS graphics audio REQUIRED)
add_executable(dungeon ${SOURCES})
target_link_libraries(dungeon sfml-graphics sfml-audio)

set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)